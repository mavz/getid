package helpers

func StringInSlice(a uint64, list []uint64) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

//a := []string{"a", "b", "c", "d"}
//b := []string{"c", "d", "e", "f"}

//fmt.Println("left", getIntersection(a, b, 1))
//fmt.Println("right", getIntersection(a, b, 2))
//fmt.Println("left & right", getIntersection(a, b, 3))
//fmt.Println("left ^ right", getIntersection(a, b, 4))
//left [a b]
//right [e f]
//left & right [c d]
//left ^ right [a b e f]
func GetIntersection(a []uint64, b []uint64, mode byte) []uint64 {
	m := make(map[uint64]byte)

	for _, k := range a {
		m[k] += 1
	}

	for _, k := range b {
		m[k] += 2
	}

	result := []uint64{}

	if mode == 4 {
		for k, v := range m {
			if v < 3 {
				result = append(result, k)
			}
		}
	} else {
		for k, v := range m {
			if v == mode {
				result = append(result, k)
			}
		}
	}

	return result
}
