package repository

import (
	"bytes"
	"crypto/sha1"
	"encoding/binary"
	"fmt"

	"bitbucket.org/mavz/getid/pkg/model"
	"github.com/dgraph-io/badger"
)

type IdsBadger struct {
	db *badger.DB
}

func NewIdBadger(db *badger.DB) *IdsBadger {
	return &IdsBadger{db: db}
}

//func (r *IdsBadger) GetId(tags model.IdSearsh) (uint64, error) {
//	return "", nil
//}

func (r *IdsBadger) SearchTag(tag *model.Tags) error {
	//переводим поисковую строку в хеш
	// объеденяем поисковое выражение с названием поля/тэга
	hash := sha1.New()
	hash.Write([]byte(tag.TagName + tag.Value))
	tag.TagHesh = fmt.Sprintf("%x", hash.Sum([]byte("")))

	var valCopy []byte

	err := r.db.View(func(txn *badger.Txn) error {
		item, err := txn.Get([]byte(tag.TagHesh))
		if err != nil {
			return err
		}
		valCopy, err = item.ValueCopy(nil)
		return err
	})
	if err != nil {
		//logrus.Info("ошибка после поиска err  Key not found", err)
		//return err
	}
	rbuf := bytes.NewReader(valCopy)
	r64 := make([]uint64, (len(valCopy)+7)/8)
	err = binary.Read(rbuf, binary.LittleEndian, &r64)
	if err != nil {
		fmt.Printf("binary.Read failed:", err)
	}
	////TODO перелапатить массив в слайс
	tag.Ids = r64
	//tag.Ids = []uint64{}
	//for _, v := range r64 {
	//	tag.Ids = append(tag.Ids, v)
	//}
	return nil
}

func (r *IdsBadger) WriteTag(tag *model.Tags) error {

	buf := new(bytes.Buffer)

	ids := tag.Ids

	err := binary.Write(buf, binary.LittleEndian, ids)
	if err != nil {
		fmt.Println("binary.Write failed:", err)
	}

	err = r.db.Update(func(txn *badger.Txn) error {

		return txn.Set([]byte(tag.TagHesh), buf.Bytes())
	})
	if err != nil {
		return err
	}
	return nil
}

func (r *IdsBadger) GetUUID(id uint64) (string, error) {
	//logrus.Info("ищем id", id)
	var valCopy []byte
	b := make([]byte, 8)
	binary.LittleEndian.PutUint64(b, uint64(id))
	err := r.db.View(func(txn *badger.Txn) error {
		item, err := txn.Get(b)
		if err != nil {
			return err
		}
		valCopy, err = item.ValueCopy(nil)
		return err
	})
	if err != nil {
		//logrus.Info("ошибка после поиска err  Key not found", err)
		//return err
	}
	return string(valCopy), nil
}

func (r *IdsBadger) SetUUID(id uint64, uuid string) error {
	//logrus.Info("сохраняем  id", id, " ", uuid)
	b := make([]byte, 8)
	binary.LittleEndian.PutUint64(b, uint64(id))
	err := r.db.Update(func(txn *badger.Txn) error {

		return txn.Set(b, []byte(uuid))
	})
	if err != nil {
		//logrus.Info("не получиилось сохранить id ", id, " ", uuid)
		return err
	}
	return nil
}
