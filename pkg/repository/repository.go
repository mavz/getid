package repository

import (
	"bitbucket.org/mavz/getid/pkg/model"
	"github.com/dgraph-io/badger"
)

type Authorization interface {
	CreateUser(user model.User) (int, error)
	GetUser(username, password string) (model.User, error)
}

type Ids interface {
	SearchTag(t *model.Tags) error
	WriteTag(tag *model.Tags) error
	GetUUID(id uint64) (uuid string, err error)
	SetUUID(id uint64, uuid string) error
}
type Repository struct {
	Ids
}

func NewRepository(db *badger.DB) *Repository {
	return &Repository{
		Ids: NewIdBadger(db),
	}
}
