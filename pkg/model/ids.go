package model

import "bitbucket.org/mavz/getid/pkg/helpers"

type RequestId struct {
	Number   string              `json:"number"`
	Univocal []map[string]string `json:"univocal"`
	Group    []map[string]string `json:"group"`
}

type ResponseId struct {
	Number string `json:"number"`
	Id     string `json:"id"`
}

type IdSearsh struct {
	Number   string
	Univocal []GroupTags
	Group    []GroupTags
	RateId   map[uint64]int
}

type GroupTags struct {
	Tags   []Tags
	RateId map[uint64]int
}

type Tags struct {
	TagName string
	TagHesh string
	Value   string
	Ids     []uint64
}

//формат хранения записи в key-value хранилище
type IdsValue struct {
	Tag   string   `json:"tag"`
	Value string   `json:"value"`
	Ids   []uint64 `json:"ids"`
}

//добавляет к Ids  новый id
func (t *Tags) AddIdTag(id uint64) (isNew bool) {
	//проверяем наличие id в массиве
	if helpers.StringInSlice(id, t.Ids) {
		return false
	}
	//добавляем id к массиву
	t.Ids = append(t.Ids, id)
	return true
}
