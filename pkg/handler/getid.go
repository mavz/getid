package handler

import (
	"net/http"

	"bitbucket.org/mavz/getid/pkg/model"
	"github.com/gin-gonic/gin"
)

func (h *Handler) getId(c *gin.Context) {

	var input model.RequestId
	if err := c.BindJSON(&input); err != nil {
		newErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}
	var idSearch model.IdSearsh

	for _, v := range input.Univocal {
		var univocalTags model.GroupTags

		for k2, v2 := range v {
			tag := &model.Tags{
				TagName: k2,
				Value:   v2,
			}
			univocalTags.Tags = append(univocalTags.Tags, *tag)
		}
		idSearch.Univocal = append(idSearch.Univocal, univocalTags)
	}
	for _, v := range input.Group {
		var groupTags model.GroupTags

		for k2, v2 := range v {
			tag := &model.Tags{
				TagName: k2,
				Value:   v2,
			}
			groupTags.Tags = append(groupTags.Tags, *tag)
		}
		idSearch.Group = append(idSearch.Group, groupTags)
	}
	id, err := h.services.Ids.GetId(&idSearch)
	//clogrus.Info("структура обработана и заполнена", idSearch)
	if err != nil {
		newErrorResponse(c, http.StatusNotFound, err.Error())
		return
	}
	c.JSON(http.StatusOK, id)
}
