package handler

import (
	"bitbucket.org/mavz/getid/pkg/service"

	"github.com/gin-gonic/gin"
)

type Handler struct {
	services *service.Service
}

func NewHandler(services *service.Service) *Handler {
	return &Handler{services: services}
}

func (h *Handler) InitRoutes() *gin.Engine {
	router := gin.New()

	auth := router.Group("/auth")
	{
		//auth.POST("/sing-up", h.singUp)
		auth.POST("/sing-in", h.singIn)
	}
	/**/
	api := router.Group("/api", h.userIdentity)
	{
		id := api.Group("/getid")
		{
			id.POST("/", h.getId)
		}
		ids := api.Group("/getids")
		{
			ids.POST("/", h.getIds)
		}
	}
	/**/
	return router
}
