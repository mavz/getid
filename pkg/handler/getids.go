package handler

import (
	"net/http"

	"bitbucket.org/mavz/getid/pkg/model"
	"github.com/gin-gonic/gin"
)

func (h *Handler) getIds(c *gin.Context) {

	var inputs []model.RequestId
	if err := c.BindJSON(&inputs); err != nil {
		newErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}
	var idsSearch []model.IdSearsh
	//var input model.RequestId
	for _, input := range inputs {

		var idSearch model.IdSearsh
		idSearch.Number = input.Number
		for _, v := range input.Univocal {
			var univocalTags model.GroupTags

			for k2, v2 := range v {
				tag := &model.Tags{
					TagName: k2,
					Value:   v2,
				}
				univocalTags.Tags = append(univocalTags.Tags, *tag)
			}
			idSearch.Univocal = append(idSearch.Univocal, univocalTags)
		}
		for _, v := range input.Group {
			var groupTags model.GroupTags

			for k2, v2 := range v {
				tag := &model.Tags{
					TagName: k2,
					Value:   v2,
				}
				groupTags.Tags = append(groupTags.Tags, *tag)
			}
			idSearch.Group = append(idSearch.Group, groupTags)
		}

		idsSearch = append(idsSearch, idSearch)
	}
	//logrus.Info("данные получены", idsSearch)
	ids, err := h.services.Ids.GetIds(&idsSearch)
	//logrus.Info("структура обработана и заполнена", idSearch)
	if err != nil {
		newErrorResponse(c, http.StatusNotFound, err.Error())
		return
	}
	//logrus.Info("id -шники найдены", ids)
	c.JSON(http.StatusOK, ids)
}
