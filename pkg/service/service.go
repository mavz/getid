package service

import (
	"bitbucket.org/mavz/getid/pkg/model"
	"bitbucket.org/mavz/getid/pkg/repository"
)

type Authorization interface {
	//CreateUser(user model.User) (int, error)
	GenerateToken(username, password string) (string, error)
	ParseToken(token string) (string, error)
}

type Ids interface {
	GetId(tegs *model.IdSearsh) (string, error)
	GetIdch(tegs *model.IdSearsh, c chan model.ResponseId)
	GetIds(tegs *[]model.IdSearsh) ([]model.ResponseId, error)
}

type Service struct {
	Authorization
	Ids
}

func NewService(repos *repository.Repository) *Service {
	return &Service{
		Authorization: NewAuthService(),
		Ids:           NewIdsService(repos.Ids),
	}
}
