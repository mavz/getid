package service

import (
	"time"

	"bitbucket.org/mavz/getid/pkg/helpers"
	"bitbucket.org/mavz/getid/pkg/model"
	"bitbucket.org/mavz/getid/pkg/repository"
	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
)

type IdsService struct {
	repo repository.Ids
}

func NewIdsService(repo repository.Ids) *IdsService {
	return &IdsService{repo: repo}
}

func (s *IdsService) GetIds(atags *[]model.IdSearsh) ([]model.ResponseId, error) {
	var ids []model.ResponseId

	logrus.Info("всего запросов : ", len(*atags))
	countResponse := len(*atags)

	c := make(chan model.ResponseId) // Делает канал для связи

	for _, v := range *atags {
		tagSeach := &model.IdSearsh{
			Number:   v.Number,
			Univocal: v.Univocal,
			Group:    v.Group,
		}
		go s.GetIdch(tagSeach, c)
	}

	for i := 0; i <= countResponse+1; i++ {
		logrus.Info("i = : ", i)
		if i >= countResponse {
			//logrus.Info("лишний элемент канала ")
			close(c)
			break
		}
		responseId := <-c // Получает значение от канала
		//logrus.Info("из канала вернуло: ", responseId)
		if &responseId != nil {
			ids = append(ids, responseId)
		}

	}
	return ids, nil
}

func (s *IdsService) GetIdch(tags *model.IdSearsh, c chan model.ResponseId) {
	var id uint64 = 0
	uuidString := ""
	var responseId = model.ResponseId{}
	//заполняем структуру IdSearsh найденными id-шниками
	countId, err := s.SearchIds(tags)

	if err != nil {
		c <- responseId
	}
	//logrus.Info("id-шников найдено ", countId)
	//принимаем решение о найденном id
	if countId > 0 {
		id, err = s.Resolve(tags)
		if err != nil {
			c <- responseId
		}
	}
	//logrus.Info("id: решение ", id)
	//если ничего не нашлось, генерируем новый id
	if id == 0 {
		id = uint64(time.Now().UTC().UnixNano())
		uuidString = uuid.New().String()
		s.repo.SetUUID(id, uuidString)
	}

	//обогащаем текущие индексы полученной информацией
	s.Enrichment(id, tags)
	//logrus.Info("id: обогатили ", id)
	//if err != nil {
	//	return id, err
	//}

	uuidString, err = s.repo.GetUUID(id)
	responseId = model.ResponseId{
		Number: tags.Number,
		Id:     uuidString,
	}
	c <- responseId
}

func (s *IdsService) GetId(tags *model.IdSearsh) (string, error) {
	var id uint64 = 0
	uuidString := ""
	//заполняем структуру IdSearsh найденными id-шниками
	countId, err := s.SearchIds(tags)
	if err != nil {
		return "", err
	}
	logrus.Info("id-шников найдено ", countId)
	//принимаем решение о найденном id
	if countId > 0 {
		id, err = s.Resolve(tags)
		if err != nil {
			return "", err
		}
		uuidString, err = s.repo.GetUUID(id)
		if err != nil {
			return "", err
		}
	}

	//если ничего не нашлось, генерируем новый id
	if id == 0 {
		id = uint64(time.Now().UTC().UnixNano())
		uuidString = uuid.New().String()
		s.repo.SetUUID(id, uuidString)
	}

	//обогащаем текущие индексы полученной информацией
	s.Enrichment(id, tags)
	//if err != nil {
	//	return id, err
	//}

	return uuidString, nil
}

func (s *IdsService) SearchIds(tags *model.IdSearsh) (int, error) {
	countId := 0
	//перебираем блок уникальных значений
	for k, v := range tags.Univocal {
		for k2, _ := range v.Tags {
			err := s.repo.SearchTag(&tags.Univocal[k].Tags[k2])
			if err != nil {
				return countId, err
			}
			countId = countId + len(tags.Univocal[k].Tags[k2].Ids)
		}
	}
	//перебираем блок необязательных значений
	for k, v := range tags.Group {
		for k2, _ := range v.Tags {
			err := s.repo.SearchTag(&tags.Group[k].Tags[k2])
			if err != nil {
				return countId, err
			}
			countId = countId + len(tags.Group[k].Tags[k2].Ids)
		}

	}
	return countId, nil
}

func (s *IdsService) Resolve(tags *model.IdSearsh) (uint64, error) {
	//перебираем блок уникальных значений
	tags.RateId = make(map[uint64]int)

	for _, v := range tags.Univocal {
		//результирующий массив из пересечений массивов блоков
		idsBlock := []uint64{}
		for _, v2 := range v.Tags {
			if len(idsBlock) == 0 {
				idsBlock = v2.Ids
			} else {
				idsBlock = helpers.GetIntersection(idsBlock, v2.Ids, 3)
			}
		}
		//считаем число вхождения id в результирующем массиве
		// в блок добавляем значение как счетчик
		for _, v3 := range idsBlock {
			tags.RateId[v3]++
		}
	}
	//если нашелся только один id
	if len(tags.RateId) == 1 {
		//принимаем и возвращаем его
		for k, _ := range tags.RateId {
			//logrus.Info("нашелся только один : ", k)
			return k, nil
		}
	}

	//если в уникальных значениях нашлось хоть что-то, обогащаем результаты неуникальными
	//если уникальных не нашлось, ищем результаты заново
	resultUnivocal := true
	if len(tags.RateId) == 0 {
		resultUnivocal = false
	}

	//если колличество отличное от единицы, пробуем искать в необязательных

	for _, v := range tags.Group {
		//результирующий массив из пересечений массивов блоков
		idsBlock := []uint64{}
		for _, v2 := range v.Tags {
			if len(idsBlock) == 0 {
				idsBlock = v2.Ids
			} else {
				//logrus.Info("массив 1 ", idsBlock)
				//logrus.Info("массив 2 ", v2.Ids)
				idsBlock = helpers.GetIntersection(idsBlock, v2.Ids, 3)
			}
			//logrus.Info("результирующий итог пересечения ", idsBlock)
			//logrus.Info("кол-во id в пересечении ", len(idsBlock))
			//пересечение не сработало хотя-бы раз, читаем что ничего не нашлось
			if len(idsBlock) == 0 {
				return 0, nil
			}

		}
		//считаем число вхождения id в результирующем массиве
		// в блок добавляем значение как счетчик
		for _, v3 := range idsBlock {
			if resultUnivocal {
				// только для тех id которые уже нашлись в уникальных значениях
				_, ok := tags.RateId[v3]
				if ok {
					tags.RateId[v3]++
				}
			} else {
				tags.RateId[v3]++
			}
		}
	}

	//logrus.Info("Посчитали : ", tags.RateId)
	//определяем  чаще встречающийся и возвращаем
	var maxId uint64
	maxCount := 0

	for k, v := range tags.RateId {
		if maxCount < v {
			maxId = k
			maxCount = v
		}
	}
	return maxId, nil
}

func (s *IdsService) Enrichment(id uint64, tags *model.IdSearsh) {

	for k, v := range tags.Univocal {
		for k2, _ := range v.Tags {
			if tags.Univocal[k].Tags[k2].AddIdTag(id) {
				//id добавился и tag необходимо перезаписать
				s.repo.WriteTag(&tags.Univocal[k].Tags[k2])
			}
		}
	}
	for k, v := range tags.Group {
		for k2, _ := range v.Tags {
			if tags.Group[k].Tags[k2].AddIdTag(id) {
				//id добавился и tag необходимо перезаписать
				s.repo.WriteTag(&tags.Group[k].Tags[k2])
			}
		}
	}
	//return nil
}
