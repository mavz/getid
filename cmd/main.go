package main

import (
	"context"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/dgraph-io/badger"
	"github.com/joho/godotenv"
	_ "github.com/lib/pq"
	"github.com/sirupsen/logrus"

	"bitbucket.org/mavz/getid"
	"bitbucket.org/mavz/getid/pkg/handler"
	"bitbucket.org/mavz/getid/pkg/repository"
	"bitbucket.org/mavz/getid/pkg/service"

	"github.com/recoilme/pudge"
	"github.com/spf13/viper"
)

func main() {
	defer pudge.CloseAll()
	logrus.SetFormatter(new(logrus.JSONFormatter))
	if err := initConfig(); err != nil {
		logrus.Fatalf("error initializing configs: %s", err.Error())
	}

	if err := godotenv.Load(); err != nil {
		logrus.Fatalf("error loading env variables: %s", err.Error())
	}

	db, err := repository.NewBadgerDB(repository.Config{
		PathDir:  viper.GetString("db.pathdir"),
		ValueDir: viper.GetString("db.valuedir"),
	})
	if err != nil {
		logrus.Fatalf("failed to initialize db: %s", err.Error())
	}
	//очистка мусора
	go badgerCleanupProc(db)

	// тестируем резервное копирование
	/*
		file, err := os.Create("/home/mavz/go/src/ALL_METADATA_SERVISE/backap/some.bak")
		writer := bufio.NewWriter(file)
		//now := time.Now().UTC().UnixNano()
		v1, err := db.Backup(writer, uint64(12000))
		logrus.Infof("резерв", v1)
		/**/
	repos := repository.NewRepository(db)
	services := service.NewService(repos)
	handlers := handler.NewHandler(services)
	srv := new(getid.Server)

	go func() {
		if err := srv.Run(viper.GetString("port"), handlers.InitRoutes()); err != nil {
			log.Fatalf("error occured while running http server: %s", err.Error())
		}

	}()
	logrus.Print("TodoApp Started")

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGTERM, syscall.SIGINT)
	<-quit

	logrus.Print("TodoApp Shutting Down")

	if err := srv.Shutdoun(context.Background()); err != nil {
		logrus.Error("error occured on server shutting down: %s", err.Error())
	}
}

func initConfig() error {
	viper.AddConfigPath("configs")
	viper.SetConfigName("config")
	return viper.ReadInConfig()
}

func badgerCleanupProc(db *badger.DB) {
	logrus.Infof("очистка мусора")
	ticker := time.NewTicker(30 * time.Minute)
	defer ticker.Stop()
	for range ticker.C {
	again:
		logrus.Infof("calling db.RunValueLogGC...")
		err := db.RunValueLogGC(0.01)

		if err == nil {
			goto again
		}
		if err != nil {
			logrus.Infof("ошибка мусорщика", err)
		}
	}
}
